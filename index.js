const express = require('express');
const app = express();
const port = 5000;
const bodyParser = require('body-parser');
const db = require('./dbConnection')
const response = require('./response')
const cors = require('cors')

app.use(bodyParser.json())
app.use(cors())

//TEST-API
app.get('/test-api/v1', (req, res)=>{
    response(200, "API Ready", "Success", res)
})

//GET-ALL-DATA-USER
app.get('/test-api/v1/user', (req, res)=>{
    const sql = "SELECT * FROM tbl_user"

    db.query(sql, (error, result)=>{
        if (error) throw error
        response(200, result, "Success", res)
    } )
})

//GET-DATA-USER-BY-ID
app.get('/test-api/v1/user/:id', (req, res)=>{
    const id = req.params.id

    const sql = `SELECT * FROM tbl_user WHERE userid = ${id}`

    db.query(sql, (error, result)=>{
        if (error) throw error
        console.log(result)
        response(200, result, "Success", res)
    } )
})

//CREATE-NEW-DATA-USER
app.post('/test-api/v1/user', (req, res)=>{
    const {namalengkap, username, password, status} = req.body

    const sql = `INSERT INTO tbl_user (namalengkap, username, password, status) 
                VALUES('${namalengkap}','${username}','${password}','${status}')`
    
    db.query(sql, (error, result)=>{
        if (error) response(500, "Invalid", "Error", res)
        if(result?.affectedRows){
            const data  = {
                isSuccess : result.affectedRows,
                id : result.insertId
            }
            response(200, data, "Success", res)
        }
    } )
})

//UPDATE-DATA-USER
app.put('/test-api/v1/user', (req, res)=>{
    const {userid, namalengkap, username, password, status} = req.body
    console.log
    const sql = `UPDATE tbl_user SET 
                    namalengkap = '${namalengkap}', 
                    username = '${username}', 
                    password = '${password}', 
                    status = '${status}' 
                WHERE userid = ${userid}`
    
    db.query(sql, (error, result)=>{
        if (error) response(500, "Invalid", "Error", res)
        if(result?.affectedRows){
            const data  = {
                isSuccess : result.affectedRows,
                message : result.message
            }
            response(200, data, "Success", res)
        }else{
            response(500, "Invalid", "Error", res)
        }
    } )
})

//DELETE-DATA-USER
app.delete('/test-api/v1/user/:id', (req, res)=>{
    const id = req.params.id

    const sql = `DELETE FROM tbl_user WHERE userid = ${id}`

    db.query(sql, (error, result)=>{
        if (error) response(500, "Invalid", "Error", res)
        response(200, result, "Success", res)
    } )
})


app.listen(port, () => {
    console.log(`this app listening on port ${port}`)
  })