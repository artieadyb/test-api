const response = (statusCode, data, message, res)=>{
    res.json(statusCode, 
        {
            status: statusCode,
            data: data,
            message,
        }
    )
}

module.exports = response;